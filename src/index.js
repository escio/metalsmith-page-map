'use strict';

module.exports = (config) => {
	config = Object.assign({
		idProperty: 'id',
		mapProperty: 'pages'
	}, config || {});

	return (files, metalsmith, done) => {
		const map = {};

		for (const filename in files) {
			if (files.hasOwnProperty(filename)) {
				const file = files[filename];
				const id = file[config.idProperty];

				// Don't map files with the id proptery missing:
				if (!id) {
					continue;
				}

				if (map[id]) {
					throw new Error(`Duplicate file id ${id}. Cannot build page map.`);
				}

				map[id] = file;
			}
		}

		const data = {};
		data[config.mapProperty] = map;

		metalsmith.metadata(Object.assign({}, metalsmith.metadata(), data));

		done();
	};
};
