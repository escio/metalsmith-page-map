# @escio/metalsmith-page-map

> Metalsmith plugin creating a map of pages keyed by a frontmatter property.

This is useful in combination with plugins like [`metalsmith-navigation`](https://www.npmjs.com/package/metalsmith-navigation) in that it allows you to look up a page by a unique ID and access its generated nav_path, or other properties in its frontmatter.

## Installation

```
$ npm install @escio/metalsmith-page-map
```

## Usage

By default the plugin will map all pages by their frontmatter property "id", if they have one, and assign the resulting map to the metadata property "pages". The property names are however configurabe.


Given pages that have the following frontmatter layout:

```yaml
id: some-page
title: My page
other: frontmatter
---
Some content
```

They can be mapped thusly:

```javascript
const metalsmith = require('metalsmith');
const pageMap = require('@escio/metalsmith-page-map');

metalsmith(
    __dirname
).use(
    pageMap({
        idProperty: 'id', // Map pages by "id" property
        mapProperty: 'pages' // Assign map to metadata property "pages"
    })
).use(
    (files, metalsmith, done) => {
        const metadata = metalsmith.metadata();

        // Log the page:
        console.log(metadata.pages['some-page']);

        done();
    }
).build((err) => {
    if (err) {
        throw err;
    }
});
```

## License

ISC © Escio AS
